const express = require('express');
const path = require('path');
const { v4: uuid } = require('uuid');
const methodOverride = require('method-override');

const random = require('random-names-places');
const movieQuote = require('popular-movie-quotes');

const app = express();
let comments = [
  {
    id: uuid(),
    author: random.name(),
    text: movieQuote.getRandomQuote(),
  },
  {
    id: uuid(),
    author: random.name(),
    text: movieQuote.getRandomQuote(),
  },
  {
    id: uuid(),
    author: random.name(),
    text: movieQuote.getRandomQuote(),
  },
  {
    id: uuid(),
    author: random.name(),
    text: movieQuote.getRandomQuote(),
  },
  {
    id: uuid(),
    author: random.name(),
    text: movieQuote.getRandomQuote(),
  },
  {
    id: uuid(),
    author: random.name(),
    text: movieQuote.getRandomQuote(),
  },
];


app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: true }));
app.use(methodOverride('_method')); 

// Homepage
app.get('/', (req, res) => {
  res.render('comments/home', {
    title: 'Homepage',
  });
});

// Index (Display all comments)
app.get('/comments', (req, res) => {
  res.render('comments/index', {
    title: 'Index',
    comments,
  });
});

// New ( Form to create new comment)
app.get('/comments/new', (req, res) => {
  res.render('comments/new', {
    title: 'Create comment',
  });
});

// Create ( Creates new comment on server )
app.post('/comments', (req, res) => {
  const { author, text } = req.body;
  const newComment = {
    id: uuid(),
    author,
    text,
  };
  comments.push(newComment);
  res.redirect('/comments');
});

// Show (Details for one specific comment)
app.get('/comments/:id', (req, res) => {
  const { id } = req.params;
  const specificComment = comments.find(comment => comment.id === id);

  res.render('comments/show', {
    ...specificComment,
    title: 'Show',
  });
});

// Form to edit a specific comment
app.get('/comments/:id/edit', (req, res) => {
  const { id } = req.params;
  const specificComment = comments.find(comment => comment.id === id);

  res.render('comments/edit', {
    ...specificComment,
    title: 'Edit comment',
  });
});

// Update specific comment on server
app.patch('/comments/:id', (req, res) => {
  const { id } = req.params;
  const { edited } = req.body;
  const specificComment = comments.find(comment => comment.id === id);
  specificComment.text = edited;
  res.redirect(`/comments/${id}`);
});

// Delete specific comment on server
app.delete('/comments/:id', (req, res) => {
  const { id } = req.params;
  const specificComment = comments.find(comment => comment.id === id);

  comments = comments.filter(comment => comment !== specificComment);
  res.redirect('/comments');
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
